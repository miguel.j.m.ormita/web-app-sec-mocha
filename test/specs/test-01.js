//var assert = require('assert');
//var expect = require('chai').expect;
import chai from 'chai';
import NavigationTab from '../page-objects/navigation-tab';

describe('Web App Security site', function() {
    it('should display the Main Page', function() {
     
        //automation codes here
       browser.url(browser.config.baseUrl);

        //The Zero Bank page is displayed.
        const onlineBankingText = $('//div[@class="active item"]//h4[text()="Online Banking"]');
        onlineBankingText.waitForDisplayed(10000);
        //assert(onlineBankingText.isDisplayed(), 'the text is not displayed');
        expect(!onlineBankingText.isDisplayed()).to.be.true;
    
    });

    it('should display the Online Banking page', function() {
                //Click on the Online Banking tab
                const onlineBankingTab = $('//*[@id="pages-nav"]//li[@id="onlineBankingMenu"]');
                onlineBankingTab.waitForDisplayed(5000);
                onlineBankingTab.click();
           
                //The Online Banking page is displayed. 
                const accountSummaryText = $('//*[text()="Account Summary"]');
                accountSummaryText.waitForDisplayed(10000);
    });    
  });
