import LoginPage from './login-page';

class OnlineBankingPage {

    constructor() {

        try{
            this.isPageDisplayed();
        }catch(err){
            throw new Error ('The '+this.pageName+' is not displayed'+err);
        }
        
    }

    get pageName() { return 'Online Banking Page';}  
    get accountSummaryText() { return $('//*[text()="Account Summary"]'); }

    isPageDisplayed() {
        this.accountSummaryText.waitForDisplayed(10000); 
    }

    clickAccountSummary() {
        
        this.accountSummaryText.click();
        return new LoginPage();
    }

}

export default OnlineBankingPage;