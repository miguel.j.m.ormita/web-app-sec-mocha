import OnlineBankingPage from './online-banking-page';

class NavigationTab {

    constructor() {

        try{
            this.isPageDisplayed();
        }catch(err){
            throw new Error ('The '+this.pageName+' is not displayed'+err);
        }
        
    }

    get pageName() { return 'Navigation tab';}  
    get navigationTab() { return $('//div[@id="nav"]'); }
    get onlineBankingTab() { return $('//strong[contains(text(),"Online Banking")]'); }

    isPageDisplayed() {
        this.navigationTab.waitForDisplayed(10000); 
    }

    clickOnlineBankingTab() {

        this.onlineBankingTab.click();
        return new OnlineBankingPage();
    }

}

export default NavigationTab;