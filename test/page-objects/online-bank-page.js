
class OnlineBankingPage{

    constructor() {
        
        try{
              this.isPageDisplayed();
        }catch(err){
            throw new Error('The '+this.pageName+' is not displayed' + err);
        }
    
    }
    
    get pageName() { return 'Online Banking Page'; }
    get activeOnlineBankingTab() { return $('//li[@class="active"][@id="onlineBankingMenu"]'); }

    isPageDisplayed(){
        this.activeOnlineBankingTab.waitForDisplayed(10000);
    }

}

export default OnlineBankingPage;
